package activity;


public class Circle implements Shape {
    private double radius;

    public Circle(double radius)
    {
        this.radius=radius;
    }
    
    public double getRadius()
    {
        return radius;
    }

    @Override
    public double getArea()
    {
        return Math.PI*(this.radius*this.radius);
    }

    @Override
    public double getPerimeter()
    {
       return 2*Math.PI*this.radius;     
    }

}
