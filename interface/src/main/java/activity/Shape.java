package activity;


public interface Shape {
    
    double getArea();
    double getPerimeter();
    
}
