package activity;

public class LotsOfShapes {
    public static void main(String[]args)
    {
        Shape[] shapes=new Shape[5];
        shapes[0]=new Rectangle(2, 3);
        shapes[1]=new Rectangle(2, 4);
        shapes[2]=new Circle(3);
        shapes[3]=new Circle(4);
        shapes[4]=new Square(2);

        for(int i=0;i<5;i++)
        {
            System.out.println("new shape");
            System.out.println(shapes[i].getArea());
            System.out.println(shapes[i].getPerimeter());
        }


    }
    
}
